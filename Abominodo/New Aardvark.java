import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Aardvark {
    private String playerName;
    private List<Domino> dominos;
    private List<Domino> guesses;
    private int[][] grid = new int[7][8];
    private int[][] guessGrid = new int[7][8];
    private int mode = -1;
    private int currentField;
    private int score;
    private long startTime;

    private PictureFrame pictureFrame = new PictureFrame();

    private static final int ROW_COUNT = 7;
    private static final int COLUMN_COUNT = 8;

    public Aardvark() {
        playerName = "";
        dominos = new ArrayList<>();
        guesses = new ArrayList<>();
    }

    public void run() {
        IOSpecialist io = new IOSpecialist();

        System.out.println("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe");
        System.out.println("Version 1.0 (c), Kevan Buckley, 2010");
        System.out.println();
        System.out.println(MultiLinugualStringTable.getMessage(0));
        playerName = io.getString();

        System.out.printf("%s %s. %s", MultiLinugualStringTable.getMessage(1),
                playerName, MultiLinugualStringTable.getMessage(2));

        int choice = -9;
        while (choice != 0) {
            System.out.println();
            String mainMenuHeader = "Main menu";
            String mainMenuUnderline = mainMenuHeader.replaceAll(".", "=");
            System.out.println(mainMenuUnderline);
            System.out.println(mainMenuHeader);
            System.out.println(mainMenuUnderline);
            System.out.println("1) Play");
            System.out.println("2) View high scores");
            System.out.println("3) View rules");
            System.out.println("0) Quit");

            choice = readIntegerInput(io);

            switch (choice) {
                case 0:
                    if (dominos.isEmpty()) {
                        System.out.println("It is a shame that you did not want to play");
                    }
